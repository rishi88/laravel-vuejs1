@extends('master')
@section('content')

		<div class="row">
			<div class="col-md-6 col-md-offset-3">

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3> Add User </h3>
					</div>	
					<div class="col-md-8 col-md-offset-2">
						
						
						<form method="post" action="{{route('addUser')}}" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="form-group">
								<label for="name"> Name: </label>
								<input type="text" name="name" class='form-control' value="{{ old('name') }}" required="ture">
								@if ($errors->has('name'))
								    <span class="alert-danger">{{ $errors->first('name') }}</span>
								@endif
							</div>
							<div class="form-group">
								<label for="phone"> Phone: </label>
								<input type="tel" name="phone" class='form-control' value="{{ old('phone') }}" required="ture">
								@if ($errors->has('phone'))
								    <span class="alert-danger">{{ $errors->first('phone') }}</span>
								@endif
							</div>
							<div class="form-group">
								<label for="email"> Email: </label>
								<input type="text" name="email" class='form-control'  value="{{ old('email') }}" required="ture">
								@if ($errors->has('email'))
								    <span class="alert-danger">{{ $errors->first('email') }}</span>
								@endif
							</div>
							<div class="form-group">
								<label for="gender"> Gender: </label>
								@if(old('gender') == 'female')
									<input type="radio" name="gender" value="male"> Male
									<input type="radio" name="gender" value="female"  checked> Female
								@else
									<input type="radio" name="gender" value="male" checked> Male
									<input type="radio" name="gender" value="female"> Female
								@endif
	
							</div>
							<div class="form-group">
								<label for="dob"> Date of Birth: </label>
								<input type="date" name="dob" class='form-control' value="{{ old('dob') }}" required="ture">
								@if ($errors->has('dob'))
								    <span class="alert-danger">{{ $errors->first('dob') }}</span>
								@endif
							</div>
							<div class="form-group">
								<label for="biography"> Biography: </label>
								<textarea name="biography" class='form-control' required="ture"> {{ old('biography') }}</textarea>
								@if ($errors->has('biography'))
								    <span class="alert-danger">{{ $errors->first('biography') }}</span>
								@endif
							</div>
							<div class="form-group">
								<label for="image_src"> Image: </label>
								<input type="file" name="image_src" required="ture">
								@if ($errors->has('image_src'))
								    <span class="alert-danger">{{ $errors->first('image_src') }}</span>
								@endif
							</div>
							
							<button type="submit" class="btn btn-default"> Add User</button>
						</form>
					</div>
				</div>
			</div>
		</div>
@endsection